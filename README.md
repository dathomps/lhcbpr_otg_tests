# LHCbPR_OTG_TESTS
Small repo to store the testing scripts for a grid implementation of LHCbPR2.

# Recipe for Mark
I will explain here the process I follow to run a job on eprexa/b from the intial details found on https://lhcb-nightlies.web.cern.ch/periodic/ to the job completion and wrapping up.

This example is very specific to a Moore "DC_Sim10-Up08_MinBias_def" job due to both the [MooreJob_RemoveInput.py](MooreJob_RemoveInput.py) (which deactivates the normal PR jobs internal input files assignment that was observed to overwrite the ganga dataset creation) and (DC_Sim10aU1_MinBias_def_Allen_ExtractInput.py)[DC_Sim10aU1_MinBias_def_Allen_ExtractInput.py] (which is used to retrieve this internal input file list so it can be converted to an LHCbDataset). In reality these options would need to be provided by the user.

I will add the analogous versions of DC_Sim10aU1_MinBias_def_Allen_ExtractInput.py for other Moore PR jobs as well as the equivalents for Boole when I expand testing to other applications.
## How I've been running the tests:
First I simply transfer the information from a job off the PR page into [launchLHCbPRJobOTG.py](launchLHCbPRJobOTG.py), such as:
![image.png](image.png)
and then adding unique details about the name of the new directory to be made locally (which could definitely be automated with a date/time for example).
Extra info is also needed about the requested output filetypes and whether to split the job.
This process of setting the job options should be simple to automate from the [test_schedule2.xml](https://gitlab.cern.ch/lhcb-core/LHCbNightlyConf/blob/master/test_schedule2.xml#L1688) in the same or similar way that PR jobs currently operate (yet to be determined). The schedule for the test job in question looks like:
```xml
  <periodictest>
    <schedule type="week" time="05:00">Mon,Tue,Wed,Thu,Fri,Sat,Sun</schedule>
    <slot>lhcb-head</slot>
    <project>Moore</project>
    <platform>x86_64_v3-centos7-gcc11-opt+g</platform>
    <test runner="lhcbpr" group="DC_Sim10aU1_MinBias_def_Allen" env="lb-run-gaudirun|DataChallengesROOTFileHandler,PrCheckerSummaryHLT1Handler"/>
    <os_label>perf-centos7</os_label>
    <count>1</count>
  </periodictest>
```
OLD: To run launch the job, simply run
```bash
python launchLHCbPRJobOTG.py 
```

Using a new config/conf file, launch the job, simply run
```bash
python launchLHCbPRJobOTG.py CONFIGFILE 
```
where currently in this repo the config file
```bash
python launchLHCbPRJobOTG.py jobConfig_DC_Sim10aU1_MinBias_def_Allen.config
```
is available.

I think you probably have a better system for monitoring the jobs, if this is the case I've added a boolean switch in [gangaJobForDevLHCBPROTG.py](ganga gangaJobForDevLHCBPROTG.py) to switch off the ganga polling job.
## Step by Step File Explanations
### launchLHCbPRJobOTG.py
This file:
* Set's the environment, slot and application settings.
* Creates a new local dir for the job info.
* Creates the developer folder ready to be packaged up for ganga.
* Transfers the job's option files locally so ganga can access them.
* Creates a file containing the input file list if the input hasn't been specified at the settings level
* Creates a dictionary-style file containing all the settings for the job, `jobOptions.txt`
* Launches [gangaJobForDevLHCBPROTG.py](gangaJobForDevLHCBPROTG.py) with `ganga ganga gangaJobForDevLHCBPROTG.py path/to/local/dir/jobOptions.txt`

### gangaJobForDevLHCBPROTG.py
Sets up a ganga job using the settings transferred from `gangaJobForDevLHCBPROTG.py`. This includes setting up an LHCbDataset from either:
* A bookkeeping path
* A list of LFNs
* A list of PFNs 
  * By default a converter is turned on that converts these PFNs to LFNs, loading the data in as DiracFiles, allowing multiple grid sites to be used for a replicated file.
  * If this proves to be faulty, there is a switch `convertPFNToLFN` in [gangaJobForDevLHCBPROTG.py](gangaJobForDevLHCBPROTG.py), which reverts to using PhysicalFiles.
Then splits the job if requested and lines up the requested output file types to become dirac files.
The job is then submitted and [LHCbPR_OTG_GangaBackend_Poll.py](LHCbPR_OTG_GangaBackend_Poll.py) is launched with `python LHCbPR_OTG_GangaBackend_Poll.py {job.id} {jobOptions['newDir']}` to begin polling ganga. **If you don't want this polling to initialise, I've added a boolean switch in [gangaJobForDevLHCBPROTG.py](gangaJobForDevLHCBPROTG.py)**.

### LHCbPR_OTG_GangaBackend_Poll.py
Essentially runs a script [LHCbPR_OTG_GangaBackend_Script.py](LHCbPR_OTG_GangaBackend_Script.py) every 10 minutes. Waits 5 mins, launches script, waits 5 mins to see if any "success/failure files" have been made indicating the jobs are no longer running, repeats.

###  LHCbPR_OTG_GangaBackend_Script.py
Opens ganga and checks the job, counting how many completed jobs there are and resubmits a failure. If a job has been resubmitted > 5 times, that subjob is marked as failed.

For completed or partially completed jobs (as in all subjobs have been marked as failed or have completed), any root files are hadd together and the stdout and stderr is transferred to the local dir. (THIS LAST BIT IS PLANNED, waiting until we know where we want to place the results.) For output files that cannot be hadd, these will be directly transferred to the PR directory that they need to be transferred to.


