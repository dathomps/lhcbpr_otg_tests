# this job needs to periodically run to check if the ganga jobs have finished

import subprocess
import time
import sys
import os

#Poll script that runs the check every 

#poll every 5 minutes -> 300 seconds
#then wait 10 minutes for ganga completion notification, should be checking every 10 minutes
# important that hadd happens after this completion notification as this could take longer than 5 mins

jobID=int(sys.argv[-2])
jobDir=sys.argv[-1]

#construct ganga completion script


while True:
    time.sleep(300)
    subprocess.Popen(f"ganga LHCbPR_OTG_GangaBackend_Script.py {jobID} {jobDir} &> {jobDir}/gangaPollOut.txt",shell=True)
    time.sleep(300)

    #now poll for completed script
    if os.path.exists(f"{jobDir}/job{jobID}.success"):
        break
    elif os.path.exists(f"{jobDir}/job{jobID}.failed"):
        break
    elif os.path.exists(f"{jobDir}/job{jobID}.partialfailed"):
        break

