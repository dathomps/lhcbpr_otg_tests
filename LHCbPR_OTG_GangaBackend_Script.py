# this job needs to periodically run to check if the ganga jobs have finished


import time
import sys
import os
import subprocess

def merge_root_output_PR(j, merged_filepath,skipIDs=[]):

    '''
    If an output dirac file is a root file, combine them using hadd

    Attributes:
    j : Job ID
    merged_filepath : desired location of hadd output
    skipIDs : subjobs to not merge
    '''


    try:
        sjobs = j.subjobs
        if len(sjobs) == 0:
            sjobs = [j]
    except AttributeError:
        #if already broken down into subjobs
        #doesnt try and extract subjobs
        sjobs = j

    access_urls = {}
    firstSj=True
    for sj in sjobs:
        if sj.id in skipIDs:
            continue
        for df in sj.outputfiles.get(DiracFile):
            #print("Fail point #0")
            if df.namePattern[-5:].lower() != ".root":
                continue

            if firstSj:
                #print("Fail point #1")
                access_urls[df.namePattern]=[df.accessURL()[0]]
            else:
                access_urls[df.namePattern].append(df.accessURL()[0])

        if firstSj:
                firstSj=False

    #if multiple root files, loops over them.
    for filename,urls in access_urls.items():
        joined_urls=" ".join(map(str,urls))

        subprocess.Popen(f"lb-conda default hadd -fk {merged_filepath}/{filename} {joined_urls} &> {merged_filepath}/merging_{filename[:-5]}.txt",shell=True)


#job properties 
jobID=int(sys.argv[-2])
jobDir=sys.argv[-1]

#wait for ganga to update itself
time.sleep(60)
print(jobs(jobID).subjobs) #printing sometimes helps with ganga refreshing
time.sleep(60)

jobComplete=len(jobs(jobID).subjobs) #the number of jobs that need to be completed for job to have suceeded
jobFailed=0

for sj in jobs(jobID).subjobs:
    if sj.status!="completed":
        jobComplete -= 1 # job incomplete
        if (sj.status=="failed") or (sj.status=="killed"):
            #set maximum resubmission value as 5
            if sj.info.submit_counter<5:
                sj.resubmit()
            else:
                #otherwise leave it as failed
                jobFailed+=1



mergedOutputPath=jobDir #to be changed

if jobComplete==len(jobs(jobID).subjobs) and jobs(jobID).status=="completed":
    #send note that job is completing, so script isn't run again.
    with open(f"{jobDir}/job{jobID}.success","w") as f:
        pass


    
    #now merge output
    merge_root_output_PR(jobs(jobID),mergedOutputPath)

    #need to sort out path to be the correct folder,
    # can be a root://LOCATION//path/to/folder/

    ## transfer non root files, does they need to be subjob labelled? 
    ## how best to do this? depends on what normal outputs are

    # for sj in sjobs:
    #     for df in sj.outputfiles.get(DiracFile):
    #         if df.namePattern[-5:].lower() != ".root":
                

    #do we need to copy any of the input parameters or stdout?? tbc.

    #write a empty file to show that the job has completed
    with open(f"{jobDir}/job{jobID}.complete","w") as f:
        pass

#if all jobs have failed, write out as failed with some info
elif jobFailed==len(jobs(jobID).subjobs):
    with open(f"{jobDir}/job{jobID}.failed","w") as f:
        original_stdout=sys.stdout
        sys.stdout = f # Change the standard output to the file we created.
        f.write(f"Job Failed (job ID: {jobID}), printing Job description of failed jobs:\n")
        for sj in jobs(jobID).subjobs:
            if (sj.status=="failed") or (sj.status=="killed"):
                print(sj)



        sys.stdout = original_stdout


# if partial failure, combine those that have succeeded 
elif jobFailed+jobComplete == len(jobs(jobID).subjobs):
    #some jobs failed, these need skipping in combination
    skipIDList=[]
    with open(f"{jobDir}/job{jobID}.partialfailed","w") as f:
        original_stdout=sys.stdout
        sys.stdout = f # Change the standard output to the file we created.
        f.write(f"Some Jobs Failed (job ID: {jobID}), but result produced from {jobComplete}/{jobComplete+jobFailed} successful subjobs\nPrinting Job description of failed jobs:\n")
        for sj in jobs(jobID).subjobs:
            if (sj.status=="failed") or (sj.status=="killed"):
                skipIDList.append(sj.id)
                print(sj)


        sys.stdout = original_stdout
    merge_root_output_PR(jobs(jobID),mergedOutputPath,skipIDs=skipIDList)
