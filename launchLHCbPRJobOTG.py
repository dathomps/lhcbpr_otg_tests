import os
import sys
from unittest import skip
import numpy as np
import subprocess

#set up the names of the different settings that are written to file as job options
#could definitely be converted to a dictionary at this point rather than in the next file
# aimed to avoid using pkls so saving options as text file still optimal
settingsToParse = ["jobTitle","platform","nightlyFlavour","nightlySlot","PRJobOptions",
                    "PRExtractInputScript","PRRemoveInputOption",
                    "inputFiles","outputFileTypes","splitJob","application","applicationVer","newDir","newDevDir"]
#this can now be replaced with jobOptions.keys() once populated


jobOptions = {}

jobOptions={a[0].strip():a[1].strip() for a in np.loadtxt(sys.argv[-1],
                                                                            delimiter="=",dtype=str,skiprows=1)}

#auto-load most, make small adjust to some


jobOptions["nightlyFlavour"] = f"lhcb-{jobOptions['nightlyFlavour']}"

# work in progress to work out what latest currently is!
#jobOptions["nightlySlot"] = "3334"

#need to evaluate this from it's form of "[]" or "['file1','file2']"
#can use eval (not safe) or manual extraction
#jobOptions["inputFiles"] = eval(jobOptions["inputFiles"]) 


# needs to be a list of either LFNs, PFNs or one string of a BKPath
                # if empty, PRExtractInputScript must be a script that
                # will print the file list required.

###OR### no evaluate method:
jobOptions["inputFiles"] = [temp.replace("'","").replace('"','') for temp in jobOptions["inputFiles"][1:-1].split(",")] if jobOptions["inputFiles"] !="[]" else []

jobOptions["outputFileTypes"] = [temp.replace("'","").replace('"','') for temp in jobOptions["outputFileTypes"][1:-1].split(",")] if jobOptions["outputFileTypes"] !="[]" else [] 
# user needs to provide the output file types that they desire to be kept
# other than stdout and stderr which are kept automatically
# Aim is to not keep the numerous sandbox files in a gauss job

jobOptions["splitJob"] = True if jobOptions["splitJob"]=="True" else False
#true if a tuple style job, running on several nodes
#false if running a long throughput/event rate job that needs to be on one grid machine
################
if jobOptions["newDevDir"] == "DEVDIR":
    jobOptions["newDevDir"] = f"{jobOptions['application']}Dev_{jobOptions['applicationVer']}"
#name of the application "dev" directory to create in the subfolder

bashCommands=[]


#####
#now begin configuration
#####

#UNIQUE MOORE SETTINGS
###### this is a unique setting to mooreJobs
#adds the input removal option into the options list in the correct location


if jobOptions["PRRemoveInputOption"]!="":
    removeInputJobOptionLocation=2
    jobOptions["PRJobOptions"]=jobOptions["PRJobOptions"].split(" ")
    jobOptions["PRJobOptions"].insert(removeInputJobOptionLocation,jobOptions["PRRemoveInputOption"])
    jobOptions["PRJobOptions"]=" ".join(jobOptions["PRJobOptions"])
    #####


os.makedirs(jobOptions["newDir"],exist_ok=True)

#resolve the slot if it is not already a slot number
realNightlyPath = os.path.realpath((f"/cvmfs/lhcbdev.cern.ch/nightlies/"
                       f'{jobOptions["nightlyFlavour"]}/{jobOptions["nightlySlot"]}'))

jobOptions["nightlySlot"]=realNightlyPath[realNightlyPath.rfind("/")+1:]

if not os.path.exists((f"/cvmfs/lhcbdev.cern.ch/nightlies/"
                       f'{jobOptions["nightlyFlavour"]}/{jobOptions["nightlySlot"]}/{jobOptions["application"]}')):

    #Decided to not allow flavours not available on the nightlies!
    #so if this slot and flavour doesn't exist will return this error message.
    print(f'{jobOptions["nightlyFlavour"]}/{jobOptions["nightlySlot"]}/{jobOptions["application"]} not available on LHCbDev!\n...Terminating')
    sys.exit(1)

    # #need to run the lbn-install bit...

    # bashCommands.append(f"lbn-install --verbose --projects {application} --platforms {platform} --dest {newDir}/NIGHTLYDIR/{nightlyFlavour}/{nightlySlot} {nightlyFlavour} {nightlySlot} &> {newDir}/lbn-Install_Out.txt")
    # bashCommands.append(f"lb-dev --nightly-base {newDir}/NIGHTLYDIR --nightly {nightlyFlavour}/{nightlySlot} --platform {platform} --name {newDevDir} --dest-dir {newDir} {application}/{applicationVer}")
    # 
    # #and then do the lb-run but with the user area
    # if PRExtractInputScript!="" and len(inputFiles)==0:
    #     bashCommands.append(f"lb-run -c {platform} --force-platform --user-area={newDir}/NIGHTLYDIR/{nightlyFlavour}/{nightlySlot} {application}/{applicationVer} python {PRExtractInputScript} &> {newDir}/jobInput.txt")

    #how do I run the input extraction script, do i need to make??

else:
    # setup dev directory, copy the Job options to a local directory
    bashCommands.append(f"lb-dev --nightly {jobOptions['nightlyFlavour']}/{jobOptions['nightlySlot']} --platform {jobOptions['platform']} --name {jobOptions['newDevDir']} --dest-dir {jobOptions['newDir']} {jobOptions['application']}/{jobOptions['applicationVer']}")
    bashCommands.append(f"lb-run -c {jobOptions['platform']} --nightly {jobOptions['nightlyFlavour']}/{jobOptions['nightlySlot']} {jobOptions['application']}/{jobOptions['applicationVer']} cp {{{jobOptions['PRJobOptions'].replace(' ',',')}}} {jobOptions['newDir']}")
    if jobOptions['PRExtractInputScript']!="" and len(jobOptions['inputFiles'])==0:
        bashCommands.append(f"lb-run -c {jobOptions['platform']} --nightly {jobOptions['nightlyFlavour']}/{jobOptions['nightlySlot']} {jobOptions['application']}/{jobOptions['applicationVer']} python {jobOptions['PRExtractInputScript']} &> {jobOptions['newDir']}/jobInput.txt")

# if PRExtractInputScript!="" and len(inputFiles)==0:
#     with open(f"{newDir}/jobInput.txt","r") as f:
#         inputFiles=[a for a in file1.readlines()[-1][1:-1].replace(" ","").replace("'","").replace('"','').split(",")]

# convert the options to point to the local version of the option files
jobOptions["PRJobOptions"] = " ".join([f"{jobOptions['newDir']}/{a[a.rfind('/')+1:]}" for a in jobOptions['PRJobOptions'].split()])

#Then need to pass the platforms, flavours and everything to the gangaJob

# store as an 2d array initially
optionsTextFileArray=np.array([[a,str(jobOptions[a])] for a in settingsToParse],dtype=str)

np.savetxt(f"{jobOptions['newDir']}/jobOptions.txt",optionsTextFileArray,fmt="%s",delimiter=" : ")
#then save with ' : ' as delimiter so file appears as dictionary

#then launch the ganga submission
bashCommands.append(f"ganga gangaJobForDevLHCBPROTG.py {jobOptions['newDir']}/jobOptions.txt &> {jobOptions['newDir']}/GANGAOUT.txt")

# run bash commands, may be a better way of running these
subprocess.Popen(";".join(bashCommands),shell=True)

#print statement included for debugging
print("Bash Commands Run:\n","\n".join(bashCommands))
