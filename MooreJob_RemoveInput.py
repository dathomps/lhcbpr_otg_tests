'''
Specific to Moore, but not specific to job

Removes the internal set input files from setup (that are used for nominal running of PR jobs)

Also removes the default limit on number of events to run over.
'''


from Moore import options

options.input_files = []

options.evt_max = -1