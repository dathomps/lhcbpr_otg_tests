#
import subprocess
import time
import os
import sys
import numpy as np


def PFNToLFN(PFNList):
    '''
    Function to convert PFN to LFN:

    Runs a dirac command to get a PFN's metadata and extracts from the output the corresponding LFN

    Places these new LFNs into a file that is then read back in to the ganga job.

    Clean up of these files performed at the end

    Attributes:
    PFNList : List of PFNs
    '''
    diracCommand=f"lb-dirac dirac-dms-pfn-metadata {' '.join(PFNList)} &> PFNToLFNOut.txt"

    if os.path.exists("LFNOut.txt"):
        os.remove("LFNOut.txt")

    #this could definitely just be a function in a file, but allows this to be run
    # internally without extra files
    extractLFNScript = ['LFNList=[]',
                        f'with open("PFNToLFNOut.txt","r") as f:',
                        '   Lines = f.readlines()',
                        '   extractNext=False',
                        '   for line in Lines:',
                        '       line=line.strip()',
                        '       if "Successful :" in line or "Writeable :" in line:',
                        '           extractNext=True',
                        '       elif extractNext==True:',
                        '           extractNext=False',
                        '           if line.replace(" ","")[0]=="/":',
                        '               LFNList.append(line.replace(" ","").replace(":",""))',
                        f'with open("LFNOut.txt","w") as f:',
                        '   for lfn in LFNList:',
                        r'      f.write(f"{lfn}\n")']
    newLineChar="\n"

    extractLFNCommand = f"python -c '{newLineChar.join(extractLFNScript)}'"




    rc=subprocess.Popen(f"{diracCommand};{extractLFNCommand}",shell=True)
    #runs and creates LFN list

    start=0
    while not os.path.exists("LFNOut.txt"):
        if start>60*60:
            print("Failed to convert PFNs to LFNs, exiting...")
            return None
        time.sleep(5)
        start+=5

    with open("LFNOut.txt") as f:
        LFNs = f.read().splitlines()

    #cleanup
    if os.path.exists("LFNOut.txt"):
        os.remove("LFNOut.txt")
    if os.path.exists("PFNToLFNOut.txt"):
        os.remove("PFNToLFNOut.txt")

    return LFNs

convertPFNToLFN = True


# Load in the job options
jobOptions={a[0].rstrip().strip():a[1].rstrip().strip() for a in np.loadtxt(sys.argv[-1],delimiter=":",dtype=str)}


#example job setup
job = Job(name=jobOptions["jobTitle"])

myApp = GaudiExec()
myApp.directory = f'{jobOptions["newDir"]}/{jobOptions["newDevDir"]}'

job.application = myApp
job.application.options = jobOptions["PRJobOptions"].split()
#converts space separated string into a list of options



#example job setup
job.application.platform =  jobOptions["platform"]


#if no input files provided
#load the jobInputs from file.
if jobOptions["inputFiles"]=="[]":
    with open(f"{jobOptions['newDir']}/jobInput.txt","r") as f:
        for a in f.readlines():
            strippedA = a.strip("\n").strip()
            if strippedA[0]=="[" and strippedA[-1]=="]":
                inputData=strippedA[1:-1].replace(" ","").replace("'","").replace('"','').split(",")
                break
        else:
            print(f"No suitable file-list found in '{jobOptions['newDir']}/jobInput.py', aborting.",file=sys.stderr)
            sys.exit(1)
        
        #Read file list from file, removing the formatting from printing

#else read directly from the job options
else:
    inputData=jobOptions["inputFiles"][1:-1].replace(" ","").replace("'","").replace('"','').split(",")
# inputData can now either be a dataset, a list of PFNs or a list of LFNs

if len(inputData)==1:
    inputData=inputData[0]

#should be using: INPUTDATA = LHCbDataset(files = [DiracFile(lfn='/lhcb/MC/Upgrade/MDF/00070647/0000/00070647_00000011_1.mdf')])
data=None
#figure out how to setup data
if isinstance(inputData,str):
    #could be a dataset
    try:
        #if input is a bookkeeping entry
        data  = BKQuery(inputData, dqflag=['OK']).getDataset(compressed=False)
    except:
        #if input data is just one file
        inputData=[inputData]

if data is None:
    # if input files are already lfns
    if inputData[0][:6]=="/lhcb/":
        data = LHCbDataset(files=[DiracFile(lfn=a) for a in inputData])
    elif inputData[0][:4]=="lfn:":
        data = LHCbDataset(files=[DiracFile(lfn=a[4:]) for a in inputData])
    
    else:
        #input files are pfns
        if convertPFNToLFN:
            #use converter to get more grid site options
            data = LHCbDataset(files=[DiracFile(lfn=a) for a in PFNToLFN(inputData)])
        else:
            #don't
            data = LHCbDataset(files=[DiracFile(a) for a in inputData])

job.inputdata = data

#outputFileTypes

#prepare output file types
outputFileTypes=[a for a in jobOptions["outputFileTypes"][1:-1].replace(" ","").replace("'","").replace('"','').split(",")]

local=False

#Continue commenting then make MD recipe and upload to a gitlab

#Local setting for testing purposes, usually forced to false
if local:
    job.backend = Local()
    #mooreJob.splitter = SplitByFiles(filesPerJob=5)
    job.outputfiles = [LocalFile(f'*{a}') for a in outputFileTypes]+[LocalFile('stdout'), LocalFile('stderr')]    
else:
    job.backend = Dirac()

    #if job split is requested, split such that each subjob uses one file
    # (this value can be edited if jobs seem to large)
    if eval(jobOptions["splitJob"]):
        job.splitter = SplitByFiles(filesPerJob=1)
    
    #store stdout and stderr locally and any other requested output files as dirac files
    # only foreseen issue here is small files produced such as xmls being stored as dirac files?

    job.outputfiles = [DiracFile(f'*{a}') for a in outputFileTypes]+[LocalFile('stdout'), LocalFile('stderr')]
    # the std.out can be a local file or a dirac file, not sure if we want to send the std.out to the front end distro

    job.backend.diracOpts = 'j.setTag(["/cvmfs/lhcbdev.cern.ch/"])' 
    #this how to ensure lhcbdev.cern.ch is available at the used sites (forces Tier 1)

#job.do_auto_resubmit = True
#job.parallel_submit = True

job.submit()

# now run the ganga backend if wanted!
runGangaPollingBackend = False

if runGangaPollingBackend:
    subprocess.Popen(f"python LHCbPR_OTG_GangaBackend_Poll.py {job.id} {jobOptions['newDir']}",shell=True)
